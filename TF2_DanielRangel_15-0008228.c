// Aluno : Daniel Marques Rangel 15/0008228

/*
 S�ntese
   Objetivo: cadastrar, alterar,listar e exlcuir registro do candidato para presid�ncia do Brasil em arquivo bin�rio
   Entrada : Nome, sigla do partido, se j� participou em alguma elei��o e numero da legenda
   Sa�da   : arquivo bin�rio com as informa��es dos candidatos
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>
#define MAXIMO 10
#define MAX_NOME 100

typedef struct{
	char nome[MAX_NOME], siglaPartido[MAXIMO], participacao;
	int num_legenda;
}registro;

// Prot�tipos
void novoCandidato(FILE *arquivo);
void alterarDados(FILE *arquivo);
void listarCandidatos(FILE *arquivo);
void excluirArquivo(FILE *arquivo);
int verificaSigla(char *p_sigla);
int verificaLegenda(int *p_legenda);
registro* ordenaStruct(char *p_ordem,int *p_quantidade,registro *p_candidato);
void grava_arquivo(FILE *arquivo,char *p_remover,int *p_quantidade,int *p_localiza_struct,registro *p_candidato);
void validaMenu(char *p_menu);
void validaNome(char *p_nome);
void validaSigla(char *p_sigla);
void validaCaracter(char *p_caracter);
void validaLegenda(int *p_legenda);
void validaOrdem(char *p_ordem);

int main(void){
// Declara��es
	char menu;
	FILE *arquivo;
// Instru��es
	do {
		puts("Menu:\n1. NOVO Candidato\n2. ALTERAR Dados do Candidato\n3. LISTAR Todos os Candidatos Cadastrados\n4. EXCLUIR Arquivo de Dados\n5. TERMINAR Programa\n");
		puts("Digite o numero correspondente a opcao desejada.");
		menu = getch();
		validaMenu(&menu);
		system("cls");
		switch (menu){
			case '1':{
				novoCandidato(arquivo);
				break;
			}
			case '2':{
				alterarDados(arquivo);
				break;
			}
			case '3':{
				listarCandidatos(arquivo);
				break;
			}
			case '4':{
				excluirArquivo(arquivo);
			}
		}
	} while(menu != '5');
  return 0;
}

// SUBPROGRAMAS //

// Objetivo: registrar novo candidato no arquivo
// Par�metros: ponteiro do arquivo
// Retorna: nada
void novoCandidato(FILE *arquivo){
// Declara��es locais
	char repetir;
	registro candidato;
	int repetido;
// Instru��es
	do{
		puts("Digite:");
		puts("Nome do candidato:");
		fgets(candidato.nome,MAX_NOME,stdin);
		validaNome(candidato.nome);
		puts("Sigla do partido:");
		do{
			fgets(candidato.siglaPartido,MAXIMO,stdin);
			validaSigla(candidato.siglaPartido);
		}while(repetido = verificaSigla(candidato.siglaPartido));
		puts("Ja participou antes ? Digite 'S' para Sim e 'N' para Nao.\n");
		candidato.participacao = getch();
		validaCaracter(&candidato.participacao);
		puts("Numero da legenda");
		do{
			scanf("%d",&candidato.num_legenda);
			validaLegenda(&candidato.num_legenda);
		}while(repetido = verificaLegenda(&candidato.num_legenda));
		
		arquivo = fopen("candidatos.bin","ab");
		if(arquivo == NULL){
			puts("ERRO AO ABRIR ARQUIVO ! Faca o cadastro novamente.");
			getch();
		}
		else{
			fwrite(&candidato,sizeof(registro),1,arquivo);
			fclose(arquivo);
			puts("Deseja cadastra outro candidato ? Digite 'S' para Sim e 'N' para Nao");
			repetir = getch();
			validaCaracter(&repetir);
			fflush(stdin);
			system("cls");
		}
	}while(repetir == 'S');
}

// Objetivo: alterar registros do arquivo
// Par�metros: nenhum
// Retorna: nada
void alterarDados(FILE *arquivo){
// Declara��es locais
	registro candidato, *p_candidato;
	int repetido=0, confirmacao, buscaLegenda, localiza_struct, aux=0, quantidade=0;
	char opcao, remover;
// Instru��es
	arquivo = fopen("candidatos.bin","rb");
	if(arquivo == NULL){
		puts("ERRO AO ABRIR ARQUIVO !");
		getch();
	}
	else{
		if(fread(&candidato,sizeof(registro),1,arquivo) != 1){
			puts("Nenhum candidato registrado ainda.");
			getch();
			system("cls");
			fclose(arquivo);
		}
		else{
			puts("Digite a legenda do partido do candidato:");
			do{
				scanf("%d",&buscaLegenda);
				validaLegenda(&buscaLegenda);
				fflush(stdin);
				rewind(arquivo);
				while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
					if(buscaLegenda == candidato.num_legenda){
						confirmacao = buscaLegenda;
						localiza_struct = quantidade;
						puts("\nCandidato localizado.");
						getch();
						system("cls");
					}
					quantidade++;
				}
				if(buscaLegenda != confirmacao)
					puts("\nCandidato nao localizado. Tente novamente");
				rewind(arquivo);
			}while(buscaLegenda != confirmacao);
			p_candidato = (registro *) calloc(quantidade,sizeof(registro));
			while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
				*(p_candidato + aux) = candidato;
				aux++;
			}
			fclose(arquivo);
			do{
				puts("Alterar:\n1- Nome\n2- Sigla do partido\n3- Participacao anterior\n4- Legenda (remover cadastro)\n5- Salvar alteracoes e Voltar\n");
				puts("Digite o numero correspondente a opcao desejada.");
				opcao = getch();
				validaMenu(&opcao);
				system("cls");
				puts("Digite:");
				switch (opcao){
					case '1':{
						puts("Nome:");
						fgets((p_candidato + localiza_struct)->nome,MAX_NOME,stdin);
						validaNome((p_candidato + localiza_struct)->nome);
						break;
					}
					case '2':{
						puts("Sigla do partido:");
						do{
							fgets((p_candidato + localiza_struct)->siglaPartido,MAXIMO,stdin);
							validaSigla((p_candidato + localiza_struct)->siglaPartido);
						}while(repetido = verificaSigla((p_candidato + localiza_struct)->siglaPartido));
						break;
					}
					case '3':{
						puts("Participacao anterior:\n'S' - Sim 'N' - Nao\n");
						(p_candidato + localiza_struct)->participacao = getch();
						validaCaracter(&(p_candidato + localiza_struct)->participacao);
						break;
					}
					case '4':{
						puts("Deseja remover esse cadastro ? 'S' - Sim 'N' - Nao");
						remover = getch();
						validaCaracter(&remover);
					}
				}
				system("cls");
			}while(opcao != '5');
			grava_arquivo(arquivo, &remover, &quantidade, &localiza_struct,p_candidato);
		}
	}
}

// Objetivo: listar ordenadamente candidatos cadastrados
// Par�metros: ponteiro do arquivo
// Retorna: nada
void listarCandidatos(FILE *arquivo){
// Declara��es locais
	registro candidato, *p_candidato;
	int aux=0,quantidade=0;
	char ordem;
// Instru��es
	arquivo = fopen("candidatos.bin","rb");
	if(arquivo == NULL){
		puts("ERRO AO ABRIR ARQUIVO ! Nao ha nenhum arquivo. Cadastre um candidato primeiro");
		getch();
	}
	else{
		while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
			quantidade++;
		}
		if(quantidade == 0){
			puts("Nenhum candidato registrado ainda.");
			getch();
			fclose(arquivo);
		}
		else{
			p_candidato = (registro *) calloc(quantidade,sizeof(registro));
			rewind(arquivo);
			while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
				*(p_candidato + aux) = candidato;
				aux++;
			}
			fclose(arquivo);
			puts("Qual ordem voce deseja ?\n\n1- Ordem crescente   2- Ordem decrescente");
			ordem = getch();
			validaOrdem(&ordem);
			system("cls");
			p_candidato = ordenaStruct(&ordem, &quantidade, p_candidato);
			puts("NOME\t\t\t\t\tSIGLA\tPARTICIPACAO\tLEGENDA");
				puts("_________________________________________________________________________");
			for(aux=0;aux<quantidade;aux++){
				printf("%s\t\t\t",(p_candidato + aux)->nome);
				if(strlen((p_candidato + aux)->nome) <= 8) // Organiza coluna da tabela
					printf("\t\t");
				if(strlen((p_candidato + aux)->nome) > 8 && strlen((p_candidato + aux)->nome) <= 20) // Organiza coluna da tabela
					printf("\t");
				printf("%s\t",(p_candidato + aux)->siglaPartido);
				((p_candidato + aux)->participacao == 'N') ? printf("Primeira vez") : printf("Novamente");
				printf("\t%d\n",(p_candidato + aux)->num_legenda);
				puts("_________________________________________________________________________");
			}
			getch();	
			free(p_candidato);
		}
	}
	system("cls");
}

// Objetivo: excluir arquivo com as informa��es
// Par�metros: ponteiro do arquivo
// Retorna: nada
void excluirArquivo(FILE *arquivo){
// Declara��es locais
	char confirmacao;
// Instru��es
	puts("Tem certeza que quer excluir o arquivo ?\nS-sim\tN-nao\n");
	confirmacao = getch();
	validaCaracter(&confirmacao);
	if(confirmacao == 'N')
		printf("OPERACAO CANCELADA!!");
	else{
		remove("candidatos.bin");
		puts("Arquivo excluido com sucesso.");
		arquivo = fopen("candidatos.bin","wb");
		if(arquivo == NULL){
			puts("ERRO AO CRIAR ARQUIVO !");
			getch();
		}
		fclose(arquivo);
	}
	getch();
	system("cls");
}

// Objetivo: ordenar structs cadastradas
// Par�metros: ordem, quantidade e o ponteiro dos candidatos
// Retorna: ponteiro do candidato
registro* ordenaStruct(char *p_ordem,int *p_quantidade,registro *p_candidato){
// Declara��es locais
	int aux,aux2;
	registro temp;
// Instru��es
	(*p_ordem == '1') ? puts("Ordem Alfabetica: A - Z\n") : puts("Ordem Alfabetica: Z - A\n");
	for(aux=0;aux<*p_quantidade-1;aux++){
		for(aux2=aux+1;aux2<*p_quantidade;aux2++){
			if(*p_ordem == '1'){
				if(strcmp((p_candidato + aux)->nome,(p_candidato + aux2)->nome) > 0){
					temp = *(p_candidato + aux);
					*(p_candidato + aux) = *(p_candidato + aux2);
					*(p_candidato + aux2) = temp;
				}
			}
			else{
				if(strcmp((p_candidato + aux)->nome,(p_candidato + aux2)->nome) < 0){
					temp = *(p_candidato + aux);
					*(p_candidato + aux) = *(p_candidato + aux2);
					*(p_candidato + aux2) = temp;
				}
			}
		}
	}
	return p_candidato;
}

// Objetivo: verificar se a sigla do partido ja foi cadastrada anteriormente
// Par�metros: sigla do partido
// Retorna: valor logico
int verificaSigla(char *p_sigla){
// Declara��es locais
	registro candidato;
	FILE *arquivo;
	int repetido=0;
// Instru��es
	arquivo = fopen("candidatos.bin","rb");
	if(arquivo == NULL){
		arquivo = fopen("candidatos.bin","wb"); // Caso ainda nao tenha sido criado o arquivo
		if(arquivo == NULL){
			puts("ERRO AO ABRIR ARQUIVO !");
			getch();
			exit(1);
		}
		fclose(arquivo);
	}
	else{
		while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
			if(strcmp(p_sigla,candidato.siglaPartido) == 0){
				puts("Sigla digitada ja esta registrada, digite uma valida");
				repetido = 1;
			}
		}
		fclose(arquivo);
	}
	return repetido;
}

// Objetivo: verificar se a legenda do partido ja foi cadastrada anteriormente
// Par�metros: legenda do partido
// Retorna: valor logico
int verificaLegenda(int *p_legenda){
// Declara��es locais
	registro candidato;
	FILE *arquivo;
	int repetido=0;
// Instru��es
	arquivo = fopen("candidatos.bin","rb");
	if(arquivo == NULL){
		puts("ERRO AO ABRIR ARQUIVO !");
		getch();
	}
	else{
		while(fread(&candidato,sizeof(registro),1,arquivo) == 1){
			if(*p_legenda == candidato.num_legenda){
				puts("Legenda digitada ja esta registrada, digite uma valida");
				repetido = 1;
			}
		}
		fclose(arquivo);
	}	
	return repetido;
}

// Objetivo: gravar altera��es no arquivo temporario e renomea-lo apos apagar arquivo original
// Par�metros: ponteiro do arquivo, caracter remover, quantidade, localiza struct e o ponteiro dos candidatos
// Retorna: nada
void grava_arquivo(FILE *arquivo,char *p_remover,int *p_quantidade,int *p_localiza_struct,registro *p_candidato){
// Declara��es locais
	int aux;
// Instru��es
	arquivo = fopen("temporario.bin","wb");
	if(arquivo == NULL){
		puts("ERRO AO ABRIR ARQUIVO !");
		getch();
	}
	else{
		for(aux=0;aux<*p_quantidade;aux++){
			if(*p_remover == 'S' && aux == *p_localiza_struct){
				puts("Candidato removido.");
				getch();
				system("cls");
			}
			else
				fwrite((p_candidato + aux),sizeof(registro),1,arquivo);
		}
		fclose(arquivo);
		remove("candidatos.bin");
		rename("temporario.bin","candidatos.bin");
	}
	free(p_candidato);
}

// Objetivo: validar o caracter menu
// Par�metros: o caracter menu (por ref�rencia)
// Retorna:  nada
void validaMenu(char *p_menu){
// Declara��es locais
	
// Instru��es
	while(*p_menu < 49 || *p_menu > 53){
		puts("\nDigite uma opcao valida (1 a 5).");
		*p_menu = getch();
	}
}

// Objetivo: validar o nome do candidato
// Par�metros: o nome do candidato (por ref�rencia)
// Retorna:  nada
void validaNome(char *p_nome){
// Declara��es locais
	
// Instru��es
	while(strlen(p_nome) <= 1 || p_nome[0] == '\t'){
		puts("Digite um nome valido (maximo de 99 caracteres).");
		fgets(p_nome,MAX_NOME,stdin);
	}
	if(p_nome[strlen(p_nome) -1] == '\n')
		p_nome[strlen(p_nome) -1] = '\0';
	if(p_nome[strlen(p_nome) -1] == '\t')
		p_nome[strlen(p_nome) -1] = '\0';
}

// Objetivo: validar a sigla do partido
// Par�metros: a sigla do partido (por ref�rencia)
// Retorna:  nada
void validaSigla(char *p_sigla){
// Declara��es locais

// Instru��es
	while(strlen(p_sigla) <= 1 || p_sigla[0] == '\t'){
		puts("Digite uma sigla valida (maximo de 9 caracteres).");
		fgets(p_sigla,MAXIMO,stdin);
	}
	if(p_sigla[strlen(p_sigla) -1] == '\n')
		p_sigla[strlen(p_sigla) -1] = '\0';
	if(p_sigla[strlen(p_sigla) -1] == '\t')
		p_sigla[strlen(p_sigla) -1] = '\0';
	strupr(p_sigla);
}

// Objetivo: validar o caracter participa��o
// Par�metros: o caracter participa��o (por ref�rencia)
// Retorna:  nada
void validaCaracter(char *p_caracter){
// Declara��es locais
	
// Instru��es
	*p_caracter = toupper(*p_caracter);
	while(*p_caracter != 'S' && *p_caracter != 'N'){
		puts("Digite uma opcao valida ('S' ou 'N').");
		fflush(stdin);
		*p_caracter = toupper(getch());
	}
}

// Objetivo: validar o numero da legenda
// Par�metros: o numero da legenda (por ref�rencia)
// Retorna:  nada
void validaLegenda(int *p_legenda){
// Declara��es locais
	
// Instru��es
	while(*p_legenda <= 10 || *p_legenda >= 100){
		puts("Digite uma legenda valida (acima de 10 e menor que 100).");
		scanf("%d",p_legenda);
	}
}

// Objetivo: validar o caracter opcao
// Par�metros: o caracter opcao (por ref�rencia)
// Retorna:  nada
void validaOrdem(char *p_ordem){
// Declara��es locais
	
// Instru��es
	while(*p_ordem != '1' && *p_ordem != '2'){
		puts("Digite uma opcao valida (1 ou 2).");
		*p_ordem = getch();
	}
}
